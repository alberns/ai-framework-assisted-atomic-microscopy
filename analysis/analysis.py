import os
import shutil
import skimage.io
import numpy as np
import hyperspy.api as hs
import statistics
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras as keras
from tqdm import tqdm
from temnn.data.mods import local_normalize
# Peak detection
from stm.preprocess import normalize
from stm.feature.peaks import find_local_peaks, refine_peaks
from skimage.morphology import disk
from scipy.spatial import cKDTree as KDTree
from stm.feature.peaks import find_local_peaks
"""
    ***
    A SET OF FUNCTIONS FOR ANALYSIS OF BOTH SIMULATED AND
    EXPERIMENTAL DATA
    ***
"""

"""
    SIMULATION DATA ANALYSIS TOOLS
"""

def F1_percentage(F1, t=0.8):
    """
    Percentage of examples above a threshold F1 value.
   
    F1:     Array of F1 values
    t:      Threshold

    returns percentage above threshold
    """
    P = np.count_nonzero((F1 >= t))/F1.size
    P *= 100
    return P

def F1_score(tar, infer):
    """
    Calculate the F1 score (beta=1) based on the precision (P) and recall (R)
        F1 = 2*(P*R)/(P+R) = TP / (TP + 1/2(FP + FN))).
        
    tar:   Target image
    infer: Predicted image
    
    returns F1 score
    """
    return Fbeta_score(tar, infer, beta=1.0)

def Fbeta_score(tar, infer, beta=1.0):
    """
    Calculate the Fbeta score based on the precision (P) and recall (R)
        F1 = (1+beta**2) * ( P*R ) / ( (beta**2 * P) + R ).
    NOTE: Background class is neglected.
        
      tar:   Target image
      infer: Predicted image
      beta:  Weight of precision (default=1.0 - harmonic mean)
    
      returns Fbeta score
    """
    # calculate Precision
    m = tf.keras.metrics.Precision()
    if tar.shape[-1] > 1:
        # neglect background class in multiclass system
        m.update_state(tar[:,:,:,:-1], infer[:,:,:,:-1])
    else:
        m.update_state(tar, infer)
    P = m.result().numpy()
    
    # calculate Recall
    m = tf.keras.metrics.Recall()
    if tar.shape[-1] > 1:
        # neglect background class in multiclass system
        m.update_state(tar[:,:,:,:-1], infer[:,:,:,:-1])
    else:
        m.update_state(tar, infer)
    R = m.result().numpy()
    
    return (1+beta**2)*((P*R)/((beta**2 * P)+R+tf.keras.backend.epsilon()))

def mse_score(tar, infer):
    """
    Calculate the mean squared error between the target and inference
    NOTE: Background class is neglected.
        
      tar:   Target image
      infer: Predicted image
    
      returns mse
    """
    # calculate MSE
    m = tf.keras.metrics.MeanSquaredError()
    if tar.shape[-1] > 1:
        # neglect background class in multiclass system
        m.update_state(tar[:,:,:,:-1], infer[:,:,:,:-1])
    else:
        m.update_state(tar, infer)
    mse = m.result().numpy()
    
    return mse

def evaluate_examples(model, inp, tar, score='F1'):
    """
    Evalutes all examples in a dataset.
    
     model: Trained Neural Network model for prediction
     inp:   Validation input data
     tar:   Validation target data
     score: Which metric to use

     returns array of metric values
    """
    mtr = np.empty(len(inp))
    desc = "Evaluating examples"
    for i in tqdm(range(len(inp)), desc=desc):
        # make prediction
        in_tmp = np.expand_dims(inp[i],axis=0)
        infer = model.predict(in_tmp)
        tar_tmp = np.expand_dims(tar[i],axis=0)
        # calculate metric
        if score == 'F1':
            mtr[i] = F1_score(tar_tmp, infer)
        elif score == 'mse':
            mtr[i] = mse_score(tar_tmp, infer)
    return mtr

def find_examples(model, inp, tar, score='F1'):
    """
    Finds the best, worst and typical (average) example in a dataset.
    
     model: Trained Neural Network model for prediction
     inp:   Validation input data
     tar:   Validation target data

     returns: best index, worst index, typical index
    """
    mtr = np.empty(len(inp))
    desc = "Locating best, worst, and typical examples"
    for i in tqdm(range(len(inp)), desc=desc):
        # make prediction
        in_tmp = np.expand_dims(inp[i],axis=0)
        infer = model.predict(in_tmp)
        tar_tmp = np.expand_dims(tar[i],axis=0)
        # calculate metric
        if score == 'F1':
            mtr[i] = F1_score(tar_tmp, infer)
        elif score == 'mse':
            mtr[i] = mse_score(tar_tmp, infer)
    if score == 'F1':
        idx_b = np.argmax(mtr)
        idx_w = np.argmin(mtr)
    elif score == 'mse':
        idx_b = np.argmin(mtr)
        idx_w = np.argmax(mtr)
    #avg = np.sum(mtr) / len(inp)
    median = statistics.median(mtr)
    diff = abs(mtr-median)
    idx_t = np.argmin(diff)
    return idx_b, idx_w, idx_t

def plot_example(model, inp, tar, idx, peak=False):
    """
    Plots an example.
    
     model: Trained Neural Network model for prediction
     inp: validation input data
     tar: validation target data
     idx: example index
    """
    if not peak:
        inp = np.expand_dims(inp[idx],axis=0)
        tar = np.expand_dims(tar[idx],axis=0)
        out = model.predict(inp)
   
        fig, (ax1,ax2,ax3) = plt.subplots(1,3,figsize=(20,8))
        ax1.imshow(inp[0,:,:,0].T,origin='lower',cmap='gray')
        ax2.imshow(tar[0,:,:,0].T,origin='lower',cmap='gray')
        ax3.imshow(out[0,:,:,0].T,origin='lower',cmap='gray')
        fig.tight_layout()
    elif peak:
        inp = np.expand_dims(inp[idx],axis=0)
        tar = np.expand_dims(tar[idx],axis=0)
        out = model.predict(inp)
        peaks = []
        for i in range(out.shape[-1]):
            peaks.append(find_local_peaks(out[0,:,:,i],
                                          1,
                                          threshold=0.5,
                                          local_threshold=0,
                                          exclude_border=0,
                                          exclude_adjacent=True)
                        )
        
        if inp.shape[-1] == 1:
            fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(21,21))
            ax1.imshow(inp[0,:,:,0],cmap='gray')
            ax2.imshow(tar[0,:,:,:])
            ax3.imshow(out[0,:,:,:])
            fig.tight_layout()
            plt.show()
        if inp.shape[-1] == 3:
            axs[0,0].imshow(inp[0,:,:,0],origin='lower',cmap='gray',interpolation='nearest')
            axs[0,1].imshow(inp[0,:,:,1],origin='lower',cmap='gray',interpolation='nearest')
            axs[0,2].imshow(inp[0,:,:,2],origin='lower',cmap='gray',interpolation='nearest')
            axs[1,0].imshow(tar[0,:,:,:],origin='lower',interpolation='nearest')
            axs[1,1].imshow(out[0,:,:,:],origin='lower',interpolation='nearest')
            axs[1,2].imshow(inp[0,:,:,0],origin='lower',cmap='gray',interpolation='nearest')
            axs[1,2].scatter(peaks[0][:,1],peaks[0][:,0],c='r')
            axs[1,2].scatter(peaks[1][:,1],peaks[1][:,0],c='g')
            axs[1,2].scatter(peaks[2][:,1],peaks[2][:,0],c='b')
            #axs[1,2].scatter(peaks[3][:,1],peaks[3][:,0])
            axs[0,0].axis('off')
            axs[0,1].axis('off')
            axs[0,2].axis('off')
            axs[1,0].axis('off')
            axs[1,1].axis('off')
            axs[1,2].axis('off')
            fig.tight_layout()
    return fig


"""
    EXPERIMENTAL DATA ANALYSIS TOOLS
"""
cnn = None
cnndim = (-1, -1)
def predict_frame(fn, net, s, t, crop=None, peaks=True):
    global cnn, cnndim
    """
    Applies network to predict atoms in a given frame.
    This supports .dm4, .dm3, and image files.

     fn:    Filename of dateaksa
     net:   Path to trained neural network
     s:     Sampling of simulated images 
     t:     Threshold for inference peak detection
     crop:  Crop the image by (xmin,xmax,ymin,ymax)
     peaks: Detect atomic peaks? 

     Global variables ensure the neural network only
     has to be loaded once if this funciton is called
     multiple times (see predict_multi_frames).

     return: inference and detected peaks as arrays.
    """
    if fn.endswith('.dm4') or fn.endswith('*.dm3'):
        # Electron microscopy file - read with HyperSpy
        f = hs.load(fn)
        # Image size must be divisible by 8
        if crop is not None:
            img = f.data[crop[0]:crop[1],crop[2]:crop[3]]
        else:
            img = f.data
        imgdim = img.shape
        #imgdim = tuple(np.array(imgdim) // 8 * 8)
        img = img[:imgdim[0],:imgdim[1]]
    else:
        #Assume an image file, read with scikit-image
        f = skimage.io.imread(fn)
        if crop is not None:
            img = skimage.color.rgb2gray(f)[crop[0]:crop[1],crop[2]:crop[3]]
        else:
            img = skimage.color.rgb2gray(f)
        imgdim = img.shape
        #imgdim = tuple(np.array(imgdim) // 8 * 8)
        img = img[:imgdim[0],:imgdim[1]]

    # Load CNN if not yet loaded or not fitting.
    if cnndim != imgdim:
        print("Loading CNN of size {} from {}".format(imgdim, net))
        #cnn = load_CNN(cnnfile, net, image_size)[1]
        cnn = keras.models.load_model(net)
        cnndim = imgdim
        
    # Normalize the image so it can be recognized
    normrange = 12.0/s
    imgnorm = local_normalize(img, normrange, normrange)
    imgnorm.shape = (1, ) + imgdim + (1,)

    # Make the prediction
    infer = cnn.predict(imgnorm)[0,:,:,0]

    if peaks:
        # Find the peaks in the output from the CNN
        d = int(2.5/s)
        p = find_local_peaks(infer, min_distance=d, 
                                 threshold=t, exclude_border=0,
                                 exclude_adjacent=True)
#       peaks = refine_peaks(normalize(prediction), peaks, 
#                            disk(2), model='polynomial')
        return infer, p
    else:
        return infer

def predict_multiframes_tree(input, net, out, s, t, crop=None, peaks=True, filetype='.dm4'):
    """
    Applies network to predict atoms in multiple frames.
    This supports a .dm4 tree structure.

     input:     Experimental data .dm4 head folder
     net:       Path to trained neural network
     out:       Output folder
     s:         Sampling of simulated images 
     t:         Threshold for inference peak detection
     crop:      Crop the image by (xmin,xmax,ymin,ymax)
     peaks:     Detect atomic peaks? 
     filetype:  File format of experimental data

    Saves peaks and inference in dedicated folders with the
    experimental data.
    """
    if input.endswith('/'):
        input = input[:-1]
    output1 = out + '_inference'
    if not os.path.exists(output1):
        print("Creating folder", output1)
        os.makedirs(output1)
    if peaks:
        output2 = out + '_peaks'
        if not os.path.exists(output2):
            print("Creating folder", output2)
            os.makedirs(output2)
        # Keep a copy of this script for reference
        #shutil.copy2(__file__, o)

    for root, dirs, files in os.walk(input, followlinks=True):
        dirs.sort()
        files.sort()
        for f in files:
            if f.endswith(filetype):
                # Load frame
                fin = os.path.join(root, f)
                print("Analyzing", fin, flush=True)
                # Predict frame
                if peaks:
                    infer, p = predict_frame(fin, net, s, t, crop, peaks)
                else:
                    infer = predict_frame(fin, net, s, t, crop, peaks)
                # Save inference
                assert root.startswith(input)
                bname = os.path.splitext(f)[0]
                outdir = output1 + root[len(input):]
                predname = os.path.join(outdir, bname+'_inference')
                print("  -> {}.npz".format(predname), flush=True)
                if not os.path.exists(outdir):
                    os.makedirs(outdir)
                np.savez_compressed(predname, prediction=infer)
                if peaks:
                    # Save peaks
                    outdir = output2 + root[len(input):]
                    atomname = os.path.join(outdir, bname+'_peaks')
                    print("  -> {}.npy".format(atomname), flush=True)
                    if not os.path.exists(outdir):
                        os.makedirs(outdir)
                    np.save(atomname, p)

def create_truth(entry, edge):
    
    positions=entry.model.get_positions()/sampling
    
    truth=MarkerCollection(truth[:,:2])
    #template_matching(truth,'hexagonal',strain=True,match_partial=False,scale=1.42,tol=.05)

    #labels=np.array(['hexagonal']*truth.num_markers)
    #labels[np.isnan(truth.get_property('rmsd'))]='other'

    #truth.set_labels(labels)
    
    #limits=[edge,size[0]-edge,edge,size[1]-edge]
    #truth = label_edges(truth,limits)
    
    return truth
