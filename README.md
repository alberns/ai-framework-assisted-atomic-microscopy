===================================================

AI Assisted Atomic-Resolution Microscopy


===================================================

Table of contents
=================
* Technologies
* Installation
* Workflow
* Folder Structure


Technologies
============
Project is created with:

* Python version: 3.8
* Tensorflow version: 2.1

Installation
============
Installation instructions are in the file `INSTALL <OLD/INSTALL.rst>`_

Workflow
========

A general workflow has been designed that goes as follows:

* Simulate your material of interest. This first step generates the exit waves and spatial points 
  of the atoms, which are used to generate the HRTEM images. Here you should generate a dataset 
  for training the neural network and a separate dataset for validating (testing) the neural network.

* Train the neural network on the simulated training data generated in step 1.

* Validate (test) the neural network on your simulated training data and simulated validation (test) 
  data also generated in step 1.

* Finaly test the neural network on a set of experimentally obtained HRTEM images.

This general workflow has been split into two underlying workflows, where the difference lies in
the generation of images. The images can either be generated 'on the fly' or be 'precomputed'.

----------
On the fly
----------
Images are generated on the fly (otf). This means that while training, HRTEM images are generated from the
exit waves using a random selection of microscope parameters in a specified range. All images are not stored
for later use, thus during validation, images are again generated based on the exit waves and the same range
of parameters. The only data that is reused are the exit waves and parameter ranges. The same exit wave with
different microscope parameters, produces a different image.

* Key advantage: The neural network never sees the exact same image twice.

-----------
Precomputed
-----------
A set of images are generated before training and validating. This follows a more classic machine/deep 
learning workflow. After the exit waves are produced, a set of HRTEM images are generated based on the exit waves 
and random values within a specified range of microscope parameters. These images are saved in folders respective 
to the material. The neural network is then trained on the training set of images, and validated on the validation 
set of images.

* Key advantage: Training and validation is significantly faster. Training data can be reused for reproducibility.


Folder Structure
================

workflow
  Folders containing the workflow. This is where simulating, training, validating, and testing happens.
 
temnn
  Folders containing the software. This includes implemntations of neural networks, classes for generating simulated
  HRTEM images, data structures for handling the HRTEM images, scripts for calculating precision and recall, and more. 

------------------
HOW TO / Tutorials
------------------

Tutorials / HOWTOs will appear here as they are written.

* `HOW TO get this project downloaded to my computer <HOWTO/HOWTO_gitlab.rst>`_.
  
* `HOW TO analyze nanoparticle image sequences <HOWTO/HOWTO_nanoparticle_sequences.rst>`_.

* `HOW TO run on the DTU Compute GPU cluster <HOWTO/HOWTO_DTU_Compute.rst>`_.




