import sys
sys.path.insert(0, '../../')
import numpy as np
import json
import os
import shutil
import argparse
import multiprocessing
from temnn.imagesimul.makeimages import MakeImages
from temnn.data.loaddata import load
from PIL import Image
from tqdm import tqdm

parameters = {
    # Size of the images during training (x, y)
    #image_size': (216, 216),
    #'image_size': (400, 400),
    'image_size': (640, 640),
    #'image_size': (440, 440),
    #'image_size': (1200,1200),

    # Number of classes in output of network, including the background
    # class.  Setting num_classes=1 means just a single class, no
    # background.  Otherwise, num_classes should be one higher than
    # the number of actual classes, to make room for the background
    # class.
    #'num_classes': 4,
    #'num_classes': 3,
    'num_classes': 1,
    
    # Spot size of atomic columns in Angstrom
    'spotsize': 0.4,

    # Image resolution range in pixels/Angstrom
    #'sampling': (0.22, 0.24),
    'sampling': (0.07, 0.08),
    #'sampling': (0.13, 0.14),

    # Focal series if not None.  The three numbers are number of images, change in focus, random part of change.
    #'multifocus': (3, 50.0, 1.0),
    'multifocus': None,   

    ## Noise parameters 
    # Range of the logarithm (base 10) of the dose in electrons/A^2
    'log_dose': (2, 4),
    # Range of blur
    'blur': (0.1, 0.8),
    
    # Range of focal spread
    'focal_spread': (5, 20),

    ## CTF Parameters (values from microscope at Nanolab)
    # Defocus range in Angstrom
    'defocus': (-200, 0),
    # Spherical abberation in Angstrom (1 micrometer = 1e4 A)
    #'Cs': (-15e4, 15e4),
    'ctf_c30': (0, 1.5*83000),
    # 2-fold astigmatism in Angstrom
    'ctf_c12': (0, 1.5*16),
    # Axial coma in Angstrom
    'ctf_c21': (0, 1.5*410),
    # 3-fold astigmatism in Angstrom
    'ctf_c23': (0, 1.5*1220),
    # Star abberation in Angstrom
    'ctf_c32': (0, 1.5*21000),
    # 4-fold astigmatism in Angstrom
    'ctf_c34': (0, 1.5*83000),
    # 5-fold astigmatism in Angstrom
    'ctf_c45': (0, 1.5*5280000),

    ## MTF Parameters
    # Range of MTF parameters (same names as in paper, except c0 which is 1.0 in paper).
    #'mtf_c0': (0.12, 0.16),
    'mtf_c1': (-0.6, 0.2),
    'mtf_c2': (0.1, 0.2),
    'mtf_c3': (0.6, 1.8),

    # normalization distance in Angstrom
    #'normalizedistance': 12.0,

    # How many images to save in debug folder (None=none, True=all).
    'debug': 100,
    
    # If a seed to the random number generator should be used
    # Can be set to True to generate a random seed that is reused in testing run
    # or set to a specific seed that is used for reproducible training.
    # Setting it to None disable deterministic testing as well.
    'seed': [998616271, 738814483, 960996334, 235280404, 560394456],
}

if __name__  == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data_dir", help="Path to directory with exit waves.")
    parser.add_argument("labels", help="Type of label: Mask, Gaussian, Disk")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.set_defaults(train=True, test=False)
    args = parser.parse_args()
 
    # We generate different images for each epoch.  It is not necessary to
    # use all image_epochs when training, and it is possible to reuse
    # them.  In test data, this is overwritten to 1.
    image_epochs = 10

    ### *** Output Directories handling ***
    data_dir =  args.data_dir
    if args.test==True:
        data_dir += '-test'
        # Only one epoch in the test data
        image_epochs = 1
    if args.train==True:
        data_dir += '/'
    out_dir = data_dir
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    image_dir = os.path.join(out_dir, 'images_labels')
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)
    params_dir = os.path.join(out_dir, 'tem_params')
    if not os.path.exists(params_dir):
        os.makedirs(params_dir)
    debug_dir = out_dir + 'debug/'
    if not os.path.exists(debug_dir):
        os.makedirs(debug_dir)
    
    # Add parameters dictonary elements 
    parameters['image_epochs'] = image_epochs
    parameters['data_dir'] = data_dir
    parameters['debug_dir'] = debug_dir  # Make it accessible to MakeImages

    # Extract image size
    image_size = parameters['image_size']
    sampling = np.mean(parameters['sampling'])
    
    # Load data as Dataset object
    data = load(data_dir)
    print('Output directory:', out_dir)
    
    # Extract number of waves (images per epoch) 
    n = len(os.listdir(data_dir + 'wave'))
    print("Number of images {} x {} image epochs.".format(
            n, image_epochs), "- Total images: ", n*image_epochs)
    parameters['images_per_epoch'] = n
   
    ### *** Generate image stream *** 
    # Peak shape defines the shape of the label peaks. This can be
    # 'Gaussian' or 'Disk', which will optimise the loss function
    # For abTEM compatability
    multiprocessing.set_start_method('spawn')
    imagestream = MakeImages(data,
                             parameters,
                             peak_shape=args.labels,
                             seed=parameters['seed'],
                             batch=100)
    
    ## *** Output ***
    # Keep a copy of this script for reference
    shutil.copy2(__file__, out_dir)
    
    # Also store the parameters in a machine_readable file
    with open(os.path.join(out_dir, "parameters.json"), "wt") as pfile:
        json.dump(parameters, pfile, sort_keys=True, indent=4)
    
    i = 0
    for epoch in range(image_epochs):
        print("    Image epoch: {}/{}".format(epoch+1,image_epochs))
        for b in range(n):
            print("Generating image", i, "of", n*image_epochs, flush=True)
            image, label, tem_params = imagestream.next_example()
            filename = os.path.join(image_dir,
                            'image_label_{:03d}_{:04d}'.format(epoch, b))
            print('Image and label saved as ', filename, flush=True)
            np.savez_compressed(filename,
                                image=np.asarray(image).astype('float32'),
                                label=np.asarray(label).astype('int8'))
            # Also store the parameters in a machine_readable file
            with open(os.path.join(params_dir,
                        "parameters_{:03d}_{:04d}".format(epoch, b)),
                        "wt") as pfile:
                json.dump(tem_params, pfile, sort_keys=True, indent=4)
            i += 1
