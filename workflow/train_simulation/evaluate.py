import os
import json
import argparse

import time
import platform
import contextlib
import zipfile
import tempfile

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'  # or any {'0', '1', '2'}
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import tensorflow as tf
import tensorflow_addons as tfa

import numpy as np
import cupy as cp
import cupyx.scipy.ndimage as cundimage

import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt

from tensorflow import keras
from cucim.skimage import filters
from tqdm import tqdm
from datetime import datetime

def get_gzipped_model_size(file):
  # Returns size of gzipped model, in bytes.

  _, zipped_file = tempfile.mkstemp('.zip')
  with zipfile.ZipFile(zipped_file, 'w', compression=zipfile.ZIP_DEFLATED) as f:
    f.write(file)

  return os.path.getsize(zipped_file)

def keras_model_memory_usage_in_bytes(model, batch_size: int):
    """
    Return the estimated memory usage of a given Keras model in bytes.
    This includes the model weights and layers, but excludes the dataset.

    The model shapes are multipled by the batch size, but the weights are not.

    Args:
        model: A Keras model.
        batch_size: The batch size you intend to run the model with. If you
            have already specified the batch size in the model itself, then
            pass `1` as the argument here.
    Returns:
        An estimate of the Keras model's memory usage in bytes.

    """
    default_dtype = tf.keras.backend.floatx()
    shapes_mem_count = 0
    internal_model_mem_count = 0
    for layer in model.layers:
        if isinstance(layer, tf.keras.Model):
            internal_model_mem_count += keras_model_memory_usage_in_bytes(
                layer, batch_size=batch_size
            )
        single_layer_mem = tf.as_dtype(layer.dtype or default_dtype).size
        out_shape = layer.output_shape
        if isinstance(out_shape, list):
            out_shape = out_shape[0]
        for s in out_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = sum(
        [tf.keras.backend.count_params(p) for p in model.trainable_weights]
    )
    non_trainable_count = sum(
        [tf.keras.backend.count_params(p) for p in model.non_trainable_weights]
    )

    total_memory = (
        batch_size * shapes_mem_count
        + internal_model_mem_count
        + trainable_count
        + non_trainable_count
    )
    return total_memory

#########################################################
################## data generator #######################
#########################################################
class datagenerator(tf.keras.utils.Sequence):
    def __init__(self, 
            batch_size, 
            img_size,
            data_paths,
            params_paths,
            input_channels,
            output_channels
            ):
         
         self.batch_size = batch_size
         self.img_size = img_size
         self.data_paths = data_paths
         self.params_paths = params_paths
         self.input_channels = input_channels
         self.output_channels = output_channels

         assert len(self.data_paths) == len(self.params_paths)
         self.n = len(self.data_paths)
         self.indexes = np.arange(self.n)

    def on_epoch_end(self):
        'updates indexes after each epoch'
        np.random.shuffle(self.indexes)
    
    def __getitem__(self, index):
        i = self.indexes[index * self.batch_size]
        batch_data_paths = self.data_paths[i : i + self.batch_size]
        batch_params_paths = self.params_paths[i : i + self.batch_size]
        
        return self.__dataloader(self.img_size,
                batch_data_paths, batch_params_paths,
                self.input_channels, self.output_channels)
    
    def __len__(self):
        return self.n // self.batch_size

    #################### data loader ########################
    def __dataloader(self, 
            img_size,
            data_paths,
            param_paths,
            input_channels,
            output_channels):
        x = np.zeros((len(data_paths), img_size[0], img_size[1], input_channels), dtype="float32")
        y = np.zeros((len(data_paths), img_size[0], img_size[1], output_channels), dtype="uint8")
        for j, (data_path, param_path) in enumerate(zip(data_paths, param_paths)):
            with open(param_path) as json_file:
                p = json.load(json_file)
            sigma = 12.0/p['sampling']
            data = np.load(data_path)
            
            img_tmp = data['image'][:,:img_size[0],:img_size[1],:]
            lbl_tmp = data['label'][:,:img_size[0],:img_size[1],:]
            
            img_tmp = local_normalize(img_tmp, sigma, sigma) 
            x[j] = img_tmp.astype(np.float32)
            y[j] = lbl_tmp.astype(np.uint8)

        return x, y

#########################################################
################## Image processing #####################
#########################################################
def local_normalize(images, sigma1, sigma2):
    images = cp.asarray(images)
    if len(images.shape)==4:
        for i in range(images.shape[0]):
            
            B=cp.zeros_like(images[0,:,:,0])
            S=cp.zeros_like(images[0,:,:,0])
            for j in range(images.shape[3]):
                B+=cundimage.gaussian_filter(images[i,:,:,j],sigma1)
            
            for j in range(images.shape[3]):
                images[i,:,:,j] = images[i,:,:,j] - B/images.shape[3]
            
            for j in range(images.shape[3]):
                S+=cp.sqrt(cundimage.gaussian_filter(images[i,:,:,j]**2, sigma2))
            
            for j in range(images.shape[3]):
                images[i,:,:,j] = images[i,:,:,j] / (S/images.shape[3])
    else:
        images = (images-cp.min(images))/(cp.max(images)-cp.min(images))
        images = images - filters.gaussian(images,sigma1,multichannel=False)
        images = images / np.sqrt(filters.gaussian(images**2, sigma2,multichannel=False))
    
    images_np = cp.asnumpy(images)
    del images
    cp._default_memory_pool.free_all_blocks()   
    return images_np

#########################################################
################## START OF MAIN CODE ###################
#########################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("vdatf",
            help="The path and name of the folder where the validation data is placed..")
    parser.add_argument("netf",
            help="The name of the folder where the trained networks are placed.")
    args = parser.parse_args()

    #########################################################
    ################ determine number of gpus ###############
    ### if there are gpus availables they will be located and
    ### utilised
    #########################################################
    cudavar = 'cuda_visible_devices'
    if cudavar in os.environ:
        cudadev = os.environ[cudavar]
        n_gpu = len(cudadev.split(','))
        print(cudavar, '=', cudadev)
        print("found {} gpu devices".format(n_gpu))
    else:
        n_gpu = 1
    # print on which host this is running (useful for troubleshooting on clusters).
    print("{}: running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))
    ## set up multi gpu
    btch = n_gpu # set batch size to the number of gpu's
    if n_gpu > 1: # if multi-gpu parallelisation is supported
        strategy = tf.distribute.mirroredstrategy()
        strategy_scope = strategy.scope
        print("*** replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == n_gpu
    else: # otherwise run on a single gpu in serial
        strategy_scope = contextlib.nullcontext
    
    ### Load metadata and networks
    vdatf = args.vdatf #'../simulation_data/Au_fcc_mixed_24102021-test'
    with open(os.path.join(vdatf, 'parameters.json')) as json_file:
        vpar = json.load(json_file)
    # Read number of validation images
    imgdim = tuple(vpar['image_size']) # spatial dimensions of input/output
    if vpar.get('multifocus', None):
        chan_in = vpar['multifocus'][0]
    else:
        chan_in = 1 # depth of input data
    chan_out = vpar['num_classes'] # number of predicted class labels
    
    inf = args.netf #'../testing_01112021'
    keras_file = inf + '/model-0'
    base_keras_file = inf + '/model-0_base' 
    pruned_keras_file = inf + '/model-0_pruned' 
    quantized_and_pruned_tflite_file = inf + '/model-0_quantized_pruned.tflite'
    print("Size of gzipped baseline Keras model: %.2f bytes" % (get_gzipped_model_size(keras_file)))
    print("Size of gzipped baseline Keras model: %.2f bytes" % (get_gzipped_model_size(base_keras_file)))
    print("Size of gzipped pruned Keras model: %.2f bytes" % (get_gzipped_model_size(pruned_keras_file)))
    print("Size of gzipped quantized and pruned TFlite model: %.2f bytes" % (get_gzipped_model_size(quantized_and_pruned_tflite_file)))
    
    with strategy_scope():
        keras.backend.clear_session()
        model = keras.models.load_model(keras_file)
        base_model = keras.models.load_model(base_keras_file)
        pruned_model = keras.models.load_model(pruned_keras_file)
    #########################################################
    ################ Load Validation Data ###################
    ### The input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### Keras will use this to validate the network after
    ### each training epoch
    #########################################################
    input_data_dir = vdatf + '/images_labels'
    data_paths = sorted(
        [
            os.path.join(input_data_dir, fname)
            for fname in os.listdir(input_data_dir)
            if fname.endswith(".npz")
            ])[0:20]
    input_params_dir = vdatf + '/tem_params'
    params_paths = sorted(
        [
            os.path.join(input_params_dir, fname)
            for fname in os.listdir(input_params_dir)
            ])[0:20]
    print('validation data:', len(data_paths), ' samples')
    val_gen = datagenerator(
            btch, imgdim, data_paths, params_paths, chan_in, chan_out)
    
    # Get a random example to predict
    idx = np.random.randint(0,len(data_paths))
    img,lbl = val_gen.__getitem__(idx)
    
    # Warm up so that there is no latency 
    dummy = model.predict(img)
    keras.backend.clear_session()
    before = time.time()
    infer = model.predict(img)
    totaltime = time.time() - before
    print("model time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
    print("model memory usage: ", keras_model_memory_usage_in_bytes(model, btch), 'bytes')
    if tf.config.list_physical_devices('GPU'):
        # Returns a dict in the form {'current': <current mem usage>,
        #                             'peak': <peak mem usage>}
        mem_dict = tf.config.experimental.get_memory_info('GPU:0')
        print('current memory: {} bytes, peak memory: {} bytes'.format(mem_dict['current'], 
            mem_dict['peak']))

    # Warm up so that there is no latency 
    dummy = base_model.predict(img)
    keras.backend.clear_session()
    before = time.time()
    base_infer = base_model.predict(img)
    totaltime = time.time() - before
    print("base model time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
    print("base model memory usage: ", keras_model_memory_usage_in_bytes(base_model, btch), 'bytes')
    if tf.config.list_physical_devices('GPU'):
        # Returns a dict in the form {'current': <current mem usage>,
        #                             'peak': <peak mem usage>}
        mem_dict = tf.config.experimental.get_memory_info('GPU:0')
        print('current memory: {} bytes, peak memory: {} bytes'.format(mem_dict['current'], 
            mem_dict['peak']))
    
    # Warm up so that there is no latency 
    dummy = pruned_model.predict(img)
    keras.backend.clear_session()
    before = time.time()
    pruned_infer = pruned_model.predict(img)
    totaltime = time.time() - before
    print("pruned model time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
    print("pruned model memory usage: ", keras_model_memory_usage_in_bytes(pruned_model, btch), 'bytes')
    if tf.config.list_physical_devices('GPU'):
        # Returns a dict in the form {'current': <current mem usage>,
        #                             'peak': <peak mem usage>}
        mem_dict = tf.config.experimental.get_memory_info('GPU:0')
        print('current memory: {} bytes, peak memory: {} bytes'.format(mem_dict['current'], 
            mem_dict['peak']))
    
    fig, axs = plt.subplots(1,5)
    axs[0].imshow(img[0,:,:,0],cmap='gray')
    axs[1].imshow(lbl[0,:,:,0])
    axs[2].imshow(infer[0,:,:,0].astype(np.float32))
    axs[3].imshow(base_infer[0,:,:,0])
    axs[4].imshow(pruned_infer[0,:,:,0])
    
    ### Evaluate model
    model.compile(
            optimizer='adam',
            loss='binary_crossentropy',
            metrics=['accuracy',
                tf.keras.metrics.Precision(),
                tf.keras.metrics.Recall()]
            )
    score = model.evaluate(
        val_gen, verbose=1)

    F1 = 2 * (score[2]*score[3]) / (score[2]+score[3])
    print('MODEL:')
    print(f'test loss: {score[0]} / test accuracy: {score[1]} / test precision: {score[2]} / test recall: {score[3]} / test F1-score: {F1}')
    
    ### Evaluate base model
    base_model.compile(
            optimizer='adam',
            loss='binary_crossentropy',
            metrics=['accuracy',
                tf.keras.metrics.Precision(),
                tf.keras.metrics.Recall()]
            )
    score = base_model.evaluate(
        val_gen, verbose=1)

    F1 = 2 * (score[2]*score[3]) / (score[2]+score[3])
    print('BASE MODEL:')
    print(f'test loss: {score[0]} / test accuracy: {score[1]} / test precision: {score[2]} / test recall: {score[3]} / test F1-score: {F1}')

    ### Evaluate pruned model
    pruned_model.compile(
            optimizer='adam',
            loss='binary_crossentropy',
            metrics=['accuracy',
                tf.keras.metrics.Precision(),
                tf.keras.metrics.Recall()]
            )
    score = pruned_model.evaluate(
        val_gen, verbose=1)

    F1 = 2 * (score[2]*score[3]) / (score[2]+score[3])
    print('PRUNED MODEL:')
    print(f'test loss: {score[0]} / test accuracy: {score[1]} / test precision: {score[2]} / test recall: {score[3]} / test F1-score: {F1}')
    
    plt.tight_layout()
    plt.show()
    
