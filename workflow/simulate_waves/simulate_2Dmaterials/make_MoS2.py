from maker import SampleMakerDistinctColumns, main
from flake import Flake
from ase import Atoms
from ase.build import mx2
import numpy as np

class MoS2Maker(SampleMakerDistinctColumns):
    numclasses = 4
    classes = {(42,): 0, (16,16): 1, (16,):2}
    defaultclass = 3     # This class is for any other combination of atomic numbers

    def __init__(self, size, seed):
        prototype = mx2(formula='MoS2')
        self.size = size
        self.flake = Flake(prototype, size, seed)

    def make_atoms(self):
        self.flake.make()
        self.flake.rotate()
        self.flake.vacancies(0.05)
        self.flake.holes(0.05)
        self.flake.perturb_positions(0.01)
        self.flake.tilt(1)  # Only a small tilt to avoid breaking colums of S2
        return self.flake.get_atoms()
    
def makeMoS2(first_number, last_number, dir_name, npoints, sampling, seed):
    size = npoints * sampling
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    print("Number of point in wave function: {} x {}".format(npoints, npoints))
    print("Sampling: {} Å/pixel".format(sampling))
    print("System size: {:.1f} Å x {:.1f} Å".format(size, size))
    print("Seed:", seed)
    maker = MoS2Maker((size, size), seed)
    maker.run(first_number, last_number, dir_name, npoints, sampling)


if __name__ == "__main__":
    main(makeMoS2, __file__)
    
