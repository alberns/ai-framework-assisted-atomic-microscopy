import numpy as np
from asap3 import FullNeighborList
from frozendict import frozendict

def get_vibrational_amplitudes(trajectories, element, rcut, nb_element=None):
    """
    Will return a frozendict of the mean of the standard deviation of positions in
    each Cartesian direction for each unique coordination number of an element to
    a neighbouring element.
    
    Arguments:
    trajectories: List of trajectories
    element:      Int Atomic number of the element
    nb_element:   Int atomic number of the neighbouring atom. If None returns total
                  coordination number
    rcut:         Float for cut off radius for neighbouring atoms.
                  (Tip: look at the radial distribution funciton to determine this.)
    
    Return:
    va: Frozendict of the vibrational amplitudes sorted by coordination number
    """
    va = {}     # Dictionary for vibrational amplitudes
    er = {}     # Dictionary for standard error of the vibrational amplitudes
    A_cxyz = [] # This list will contain all amplitudes for coordination number (c) and cartesian
                # coordinates (xyz) of the atom
    for trajectory in trajectories:
        # Final atomic configuration
        finalatoms = trajectory[-1]
        natoms = len(finalatoms)
    
        # Obtain all cartesian positions of each atom throughout the MD steps
        allpos = np.empty(( len(trajectory), natoms, 3))
        for i, atoms in enumerate(trajectory):
            pos = atoms.get_positions()
            allpos[i] = pos
    
        # Get full neighbours list with specified cut off radius in Ångstrøm using atoms
        # with average positions form the MD simulation
        avgpos = np.mean(allpos,axis=0)
        avgatoms = finalatoms.copy()
        avgatoms.set_positions(avgpos)
        nblist = FullNeighborList(rCut=rcut, atoms=avgatoms)
    
        # Get list of atomic numbers in atoms
        N = finalatoms.get_atomic_numbers()
    
        # Standard deviation of the positions of each atom in each (xyz) cartesian direction
        # These are all the amplitudes
        A_xyz = np.std(allpos, axis=0)
        # Extract coordination number for specified element
        for i, nb in enumerate(nblist):
            if N[i] != element: # If this index is not for the element of interest
                continue
            if nb_element is not None:
                # Number of neighbouring atoms of the specified element
                Nnb = len( N[nb][ N[nb] == nb_element ] )
            else:
                # Total number of neighbouring atoms (any element)
                Nnb = len(N[nb])
            # Combine the amplitudes with the coordination number
            A_cxyz.append(np.insert(A_xyz[i],0,Nnb))
            
    # Obtain unique coordination numbers to mean over
    A_cxyz = np.asarray(A_cxyz)
    uniquecn = np.unique(A_cxyz[:,0])
    for cn in uniquecn:
        tmp_cnxyz = A_cxyz[np.where(A_cxyz[:,0] == cn)] # Grab all xyz with the specific coordination number
        va[cn] = np.mean(tmp_cnxyz,axis=0)[1:]          # Mean of amplitudes over coordination number
        
        stderr = np.std(tmp_cnxyz,axis=0,ddof=0)[1:] / np.sqrt(tmp_cnxyz.shape[0]) # Standard error
        er[cn] = np.nan_to_num(stderr) # with only one datapoint the std is not defined!
    
    va = frozendict(va)
    er = frozendict(er)
    return va, er

def perturb_atoms(atoms, rcut, ele_A, va_A, ele_B=None, va_B=None, seed=None):
    """
    Will return atoms perturbed by a Gaussian distribution based on the given vibrational amplitudes
    
    Arguments:
    atoms:  Atoms of the system of interest
    rcut:   Float for cut off radius for neighbouring atoms
    ele_A:  Atomic number of atomic species A in atoms
    va_A:   FrozenDict/Dict containing the Cartesian vibrational amplitudes for each coordination number of 
            atomic species A in atom
    ele_B:  Atomic number of atomic species B in atoms. None if there is only one species.
    va_B:   FrozenDict/Dict containing the Cartesian vibrational amplitudes for each coordination number of 
            atomic species B in atoms. None if there is only one species.
    seed:   Int/Generator Seed to reproduce random peturbations.
            
    
    Return:
    patoms: perturbed atoms
    """
    
    # Random number generator for reproducibility
    if isinstance(seed, np.random.Generator):
        rng = seed
    rng = np.random.default_rng(seed=seed)
 
    # Get the full neighbour list
    nblist = FullNeighborList(rCut=rcut, atoms=atoms)
    # Get list of atomic numbers in atoms
    N = atoms.get_atomic_numbers()
    if len(np.unique(N)) > 1 and ele_B is None:
        print("Only one element specified in multi-component system.")    

    # Extract element A from atoms
    cnA_i = [] # atom index and coordination number
    for i, nb in enumerate(nblist):
        if N[i] != ele_A: # If this index is not for the element of interest
            continue
        else:
            # Total number of neighbouring atoms
            Nnb = len(N[nb])
        cnA_i.append((i, Nnb))
    cnA_i = np.asarray(cnA_i)
    
    #perturb positions
    pos_axyz = atoms.get_positions()
    for i, c in cnA_i:
        va_xyz = va_A[c] # Extract amplitudes from dictionary
        pos_axyz[i,0] += rng.normal(loc=0.0, scale=va_xyz[0], size=None) # Perturb x
        pos_axyz[i,1] += rng.normal(loc=0.0, scale=va_xyz[1], size=None) # Perturb y
        pos_axyz[i,2] += rng.normal(loc=0.0, scale=va_xyz[2], size=None) # Perturb z
    
    # For the second species in the atomic system if present
    if ele_B is not None:    
        # Extract element B from atoms
        cnB_i = [] # atom index and coordination number
        for i, nb in enumerate(nblist):
            if N[i] != ele_B: # If this index is not for the element of interest
                continue
            else:
                # Total number of neighbouring atoms
                Nnb = len(N[nb])
            cnB_i.append((i, Nnb))
        cnB_i = np.asarray(cnB_i)
        
        for i, c in cnB_i:
            va_xyz = va_B[c] # Extract amplitudes from dictionary
            pos_axyz[i,0] += rng.normal(loc=0.0, scale=va_xyz[0], size=None) # Perturb x
            pos_axyz[i,1] += rng.normal(loc=0.0, scale=va_xyz[1], size=None) # Perturb y
            pos_axyz[i,2] += rng.normal(loc=0.0, scale=va_xyz[2], size=None) # Perturb z
    
    patoms = atoms.copy()
    patoms.set_positions(pos_axyz)
    return patoms
