## for importing modules from parent directories
import sys
sys.path.insert(0, '../../../../NeuralNetwork_HRTEM')
## import modules
import numpy as np
import matplotlib.pyplot as plt

import os
import time
import shutil
import argparse
import subprocess

from ase.io import write
from ase.io.trajectory import Trajectory

from temnn.reproducible import make_reproducible

from abtem.potentials import Potential
from abtem.waves import PlaneWave

from scipy.cluster.hierarchy import fcluster, linkage

import multiprocessing

def driver2(args):
    driver(**args)
    
def driver(first_number, last_number, trajectory, dir_name, N, sampling):
    """Create nanoparticles, their exit waves and the ground truth for training.

    This function is creating a sequence of nanoparticles.  It is
    intended to be called in multiple processes in parallel, so the
    start and end number of the sequence is specified.
    
    trajectory: string
        Trajectory path containing the MD/Pseudo-MD configurations
    dir_name: string
        Name of the folder where the output is placed.
    N: int
        Size of the wavefunction array (will be N*N 128-bit complex numbers).
    sampling: float
        Resolution of the wavefunctions, in Angstrom per pixel.
    element: str
        Chemical element of the nanoparticle
    tilt: float
        Maximal off-axis tilt of nanoparticle, in degrees.  1.0 to 2.0 are good values.
    """
    # Parameters
    L=sampling*N   # Cell size in Angstrom

    traj = Trajectory(trajectory,'r')    
    # Create the models
    # The first mention of miller indices includes the family of planes
    # The second mention allows for truncation of that specific layer
    for i in range(first_number, last_number):
        atoms = traj[i]
        atoms.set_cell((L,L,L))      
        atoms.center()

        # Find the atomic columns
        positions_xy = atoms.get_positions()[:,:2]
        clusters = fcluster(linkage(positions_xy), 1, criterion='distance')
        unique, indices = np.unique(clusters, return_index=True)
        # Find the positions of the columns and the number of atoms per column.
        sites_xy = np.array( [np.mean(positions_xy[clusters==u],axis=0) for u in unique] )
        heights = np.array( [np.sum(clusters==u) for u in unique] )

        ## *** abTEM for TEM image ***
        # Determine number of points for each potential slice
        wave_xy = np.asarray(atoms.cell[0,0], atoms.cell[1,1])
        wave_pts = int(wave_xy/sampling)

        # Build potential for atoms
        # Number of slices is set by atoms.cell[2,2] with a default thickenss of 0.5A
        potential = Potential(atoms,
                      sampling=sampling, 
                      gpts=wave_pts, 
                      parametrization='kirkland', 
                      projection='infinite')
        # Build wave
        wave = PlaneWave(
                energy=50e3     #acceleration voltage in eV
        )
        # Compute exit wave
        exitwave = wave.multislice(potential,
                                 pbar=False)

        # Save coordinates of positions, sites, and heights
        np.savez('{0}/points/points_{1:04d}.npz'.format(dir_name,i),
                    positions=positions_xy,
                    sites=sites_xy, 
                    heights=heights
                )
        exitwave.write('{0}/wave/wave_{1:04d}'.format(dir_name,i))
        write('{0}/model/model_{1:04d}.cfg'.format(dir_name,i),atoms)
        print('   Saved model {:2d} w/ {:5d} atoms'.format(i,
                                                     len(atoms),
                                                     flush = True))
    traj.close()

def_resolution=1000
def_sampling=0.05

if __name__ == "__main__":
    # Use the standard argument parser - but also save the names of the positional parameters for
    # creating the reproducibility file.
    parser = argparse.ArgumentParser()
    positionalparams = ('trajectory', 'folder')
    parser.add_argument("trajectory", help="The path to the trajectory file containing the atoms.")
    parser.add_argument("folder", help="The name of the folder (in ../simulation_data) where the output is placed.")
    parser.add_argument("--sampling", type=float, default=def_sampling,
                        help="Sampling of wave function in Angstrom per pixel")
    parser.add_argument("--resolution", type=int, default=def_resolution,
                            help="Resolution of wave function in pixels.")
    parser.add_argument("-s", "--start", type=int, default=0,
                        help="Starting value.")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.set_defaults(test=False)
    args = parser.parse_args()
    
    # Read trajectory file
    traj = Trajectory(args.trajectory,'r')    
    number = len(traj)
    traj.close()

    if args.numproc > number - args.start:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('../../', 'simulation_data', args.folder)
        
    # Make sure working folders exist
    # Generate test set?
    if args.test==True:
        dir_name += '-test'
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['wave', 'model', 'points']:
            os.makedirs(os.path.join(dir_name, subf))
           

    print("Generating samples {} to {} from {} in folder {} using {} process(es)\n".format(
        args.start, number, args.trajectory, dir_name, numproc)) 

    # Keep a copy of this script for reference ...
    shutil.copy2(__file__, dir_name)
    # ... and write a shell script to reproduce this run.
    argdict = vars(args).copy()
    argdict['numproc'] = numproc
    make_reproducible(os.path.join(dir_name, 'reproduce.sh'),
                        parser.prog,
                        argdict,
                        positionalparams)
    before = time.time()

    if numproc == 1:
        # Running on a single core.
        driver(first_number=args.start,
               last_number=number,
               trajectory=args.trajectory,
               dir_name=dir_name,
               N=args.resolution,
               sampling=args.sampling)
    else:
        # For abTEM compatability
        #multiprocessing.set_start_method('spawn')
        data = []
        ndata = number - args.start
        for i in range(numproc):
            data.append(dict(
                first_number=args.start + i * ndata // numproc,
                last_number=args.start + (i+1) * ndata // numproc,
                trajectory=args.trajectory,
                dir_name=dir_name,
                N=args.resolution,
                sampling=args.sampling,
                ))
        with multiprocessing.Pool(numproc) as pool:
            pool.map(driver2, data)
    
    print("Time to simulate waves: {:.2f} s.".format(time.time() - before))
