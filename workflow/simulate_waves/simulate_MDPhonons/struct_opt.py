#SBATCH --mail-type=ALL
#SBATCH --mail-user=mhlla@dtu.dk  # The default value is the submitting user.
#SBATCH --partition=xeon24
#SBATCH -N 1
#SBATCH -n 24
#SBATCH --time=1-02:00:00
#SBATCH --output=MoS2-optimisation-defect-2%J.out

from ase import Atoms
from ase.optimize import GPMin,BFGS,QuasiNewton
from ase.io import read
from ase.visualize import view
from gpaw import GPAW

# Read atoms from setup.ipynb as the initial trajectory
init = read('initial_defect-2.traj')

# Set up GPAW calculator
calc = GPAW(mode='lcao',    # Linear combination of atomic orbitals basis
            basis='dzp',    # double-zeta-polarised
            xc='PBE',       # PBE exchange-correlation funcitonal
            txt='GPAW_defect-2.txt'  # Output file for initial GPAW calculation
            )
init.calc = calc

# Set up relaxation, I have chosen to use Gaussian process assisted BFGS 
# for machine learning based speed up
relax = QuasiNewton(init,
             trajectory='relax_defect-2.traj',   # Save trajectories
             logfile='optimisation_defect-2.txt' # Output file for relaxation
             )
#relax = GPMin(init,
#             trajectory='relax_defect.traj',   # Save trajectories
#             logfile='optimisation_defect.txt' # Output file for relaxation
#             )
relax.run() # Run relaxation
