#!/bin/bash

#!/bin/bash

if [[ -z "$CONDA_DEFAULT_ENV" ]]; then
    echo "Please activate the right Anaconda envronment before running this script." 1>&2
    exit
fi

if [[ -z "$CUDA_VISIBLE_DEVICES" ]]; then
    echo "Please select the desired GPU by setting CUDA_VISIBLE_DEVICES." 1>&2
    exit
fi

SIMULSUBFOLDER=NeuralNetwork_HRTEM_simulation_data/Au_cluster-110-2deg5-big
SIMULFOLDER=/scratch/$USER/$SIMULSUBFOLDER
# Use this to e.g. exclude the wave subfolder when using precomputed images
RSYNCOPT='--exclude=wave --exclude=points --exclude=model'

mkdir -p /scratch/$USER/$SIMULSUBFOLDER
mkdir -p /scratch/$USER/$SIMULSUBFOLDER-test
rsync -av $RSYNCOPT transfer.gbar.dtu.dk:/work3/$USER/$SIMULSUBFOLDER/. $SIMULFOLDER/.
rsync -av $RSYNCOPT transfer.gbar.dtu.dk:/work3/$USER/$SIMULSUBFOLDER-test/. $SIMULFOLDER-test/.

# Trick the script into only using 10 cpus (be nice to other users!)
export LSB_MAX_NUM_PROCESSORS=10
python learningcurve_precomputed.py ../MSDnet_precomputed_trained_data/Au_cluster-110-2deg5-big/

