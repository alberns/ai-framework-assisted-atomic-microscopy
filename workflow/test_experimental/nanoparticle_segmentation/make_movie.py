import glob
from moviepy.editor import *

output_dir = '4K_segmentation_output'

individual_frames = glob.glob('{}/frame_*.png'.format(output_dir))
print('Number of frames:', len(individual_frames))

clip = ImageSequenceClip(individual_frames, fps = 4)

clip.write_videofile('{}/video.avi'.format(output_dir),codec='png')
