import sys
sys.path.insert(0, '../../../')

import os
import glob

import hyperspy.api as hs
import cv2

import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
import tensorflow.keras as keras

from temnn.data.mods import local_normalize

from stm.feature.peaks import find_local_peaks

from codetiming import Timer
from tqdm import tqdm

### Functions ###

def expand_mask(mask, radius=10):
    expand = mask.copy()
    for j in range(radius,mask.shape[1]):
        for k in range(radius,mask.shape[2]):
            window = mask[0,j-radius:j+radius,k-radius:k+radius]
            fraction = np.count_nonzero(window == 1.0)/ \
                    (window.shape[0]*window.shape[1])
            if fraction > 0.1:
                expand[0,j,k] = 1.0
    return expand

def crop_nanoparticle(img, infer):
    # Make the inference mask binary
    binary = infer[:,:,:,0].copy()
    binary[binary > 0.01] = 1.0
    
    # Expand the inference mask
    crop = expand_mask(binary)
    blur_level=200
    crop = cv2.blur(crop[0,:,:], (blur_level, blur_level))
    crop = crop.reshape((1,)+crop.shape+(1,))
    
    # Crop out nanoparticle
    img_crop = img*crop
    return img_crop, crop

def fft(img, res):
    f = np.fft.fft2(img)
    f = np.fft.fftshift(f)
    cf = f.shape[0]/2,f.shape[1]/2
    f = f[int(cf[0]-res/2):int(cf[0]+res/2),
            int(cf[1]-res/2):int(cf[1]+res/2)]
    return np.log(np.abs(f)**2)

def block_centre(img):
    center = img.shape[0]/2,img.shape[1]/2
    blocked = img.copy()
    blocked[int(center[0]-7):int(center[0]+7), 
            int(center[1]-7):int(center[1]+7)] = 0.0
    return blocked

def rot_mat(theta):
    r = np.array(( (np.cos(theta), -np.sin(theta)),
               (np.sin(theta),  np.cos(theta)) ))
    return r

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    if phi < 0:
        phi += 2*np.pi
    return rho, phi

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return x, y

###########

folder='../../experimental_data/wibang/20210316_ETEM_MEMS8/AuCeO2_defoc9'
files=glob.glob(r'{}/Hour_00/Minute_*/Second_*/*.dm4'.format(folder))
outfolder='4K_segmentation_output'
if not os.path.exists(outfolder):
    os.mkdir(outfolder)
print('Files contain {} frames.'.format(len(files)))

### Load trained neural network
n = 'MSDnet'
network = 'Au_fcc_mixed'
#Trained Neural net folder
nnf = '../../{}_precomputed_trained_data/{}'.format(n,network) 
nnf = glob.glob(os.path.join(nnf,'model-*'))[-1] # latest model
print("Using {} CNN model in".format(n), network)
mod = keras.models.load_model(nnf)

t = Timer(name='4K Segmentation of {} frames.'.format(len(files)),
        text='{name}: Elapsed time: {:.4f} seconds')
t.start()
for fidx, f in enumerate(files):
    ### Load data
    d = hs.load(f)
    print(f)

    x_cal, y_cal=d.axes_manager.signal_axes[0].scale, \
            d.axes_manager.signal_axes[1].scale
    if x_cal > 0.008 or x_cal < 0.007:
        continue

    fullres = 4096
    winres = 512
    ftres = 160
    
    nw = int(fullres/winres)
    seg = np.zeros((1,) + d.data.shape + (1,))
    ft = np.zeros((ftres*nw,ftres*nw))
    
    assert x_cal == y_cal
    normalizerange = 12.0/(x_cal*10)
    print('Normalising input...')
    tt = Timer(name='Normalisation of frame {}'.format(str(fidx).zfill(3)),
            text='{name}: Elapsed time: {:.4f} seconds')
    tt.start()
    imgn = local_normalize(d.data, normalizerange, normalizerange)
    tt.stop()
    imgn.shape = (1,) + imgn.shape + (1,)
    for x in tqdm(range(nw),desc='Column',position=0):
        for y in tqdm(range(nw),desc='Row',position=1,leave=False):
        
            img = imgn[:, x*winres:(x+1)*winres,
                    y*winres:(y+1)*winres, :]
            #img = local_normalize(d.data[x*winres:(x+1)*winres, \
            #                              y*winres:(y+1)*winres],
            #                          normalizerange, normalizerange)
            #img.shape = (1,) + img.shape + (1,)
            infer = mod.predict(img)
            
            # Cut out nanoparticle
            imgcrop, crop = crop_nanoparticle(img, infer)
            # Get FT
            imgcrop_ft = fft(imgcrop[0,:,:,0],ftres)
            # Locate peaks
            imgcrop_ftnorm = block_centre(imgcrop_ft)
            peaks = find_local_peaks(imgcrop_ftnorm,
                                     min_distance=30,
                                     threshold=0.8,
                                     local_threshold=0,
                                     exclude_adjacent=True)
            
            if len(peaks) <= 8: # Discard images dominated by noise with many peaks
                # Locate the centre of the points to centre the origin
                centroid = (sum(peaks[:,0]) / len(peaks), \
                        sum(peaks[:,1]) / len(peaks))
                cpeaks = peaks - centroid
                num_peaks = len(cpeaks)
                
                # Analyse spectrum
                polarpeaks = np.zeros(cpeaks.shape)
                # Convert all peaks from cartesian to polar coordinates
                for i, peak in enumerate(cpeaks):
                    polarpeaks[i,0], polarpeaks[i,1] = cart2pol(peak[0],peak[1])
                # Sort the peaks by increasing angle
                polarpeaks = np.array(sorted(polarpeaks,key=lambda x: x[1]))
                b = [] # this will store the brightness of 2 symmetric peaks
                for i, polpeak1 in enumerate(polarpeaks):
                    # For every peak, loop through all other peaks to find a symmetric peak
                    for j, polpeak2 in enumerate(polarpeaks[i+1:]):
                        # This if statement matches the length and symmetry (angle+Pi) of every point 
                        # to find a symmetric pair
                        if (
                            polpeak1[0] >= polpeak2[0]-0.01 and
                            polpeak1[0] <= polpeak2[0]+0.01 and 
                            polpeak1[1]+np.pi >= polpeak2[1]-0.01 and
                            polpeak1[1]+np.pi <= polpeak2[1]+0.01
                            ):

                            cartp1_x, cartp1_y = pol2cart(polpeak1[0],polpeak1[1])
                            cartp2_x, cartp2_y = pol2cart(polpeak2[0],polpeak2[1])
                            # Average value of pixels around the peak, normalised by the average value of pixels
                            # at a larger area around the peak (a #normalised brightness")
                            nb = np.mean(
                                    imgcrop_ft[int(cartp1_y+centroid[1]-2):int(cartp1_y+centroid[1]+2), 
                                        int(cartp1_x+centroid[0]-2):int(cartp1_x+centroid[0]+2)]
                                    ) / \
                                 np.mean(
                                    imgcrop_ft[int(cartp1_y+centroid[1]-10):int(cartp1_y+centroid[1]+10), 
                                        int(cartp1_x+centroid[0]-10):int(cartp1_x+centroid[0]+10)]
                                    )
    
                            b.append(nb)
                # If there is a single symmetric pair, then there is a more pronounced periodicity in
                # that direction. These are not well-aligned NP's, but still a region of interest
                if len(b) == 1:
                    br = 0.5
                # Else if there are 2 or 3 symmetric pairs (i.e. 4 or 6 pronounced peaks) we have a NP that is 
                # in the zone axis. Comparing the brightness of the brightest peak to the weakest peaks, gives a
                # measure of that alignment (i.e. if the ratio is 1 there is an even periodicity in all periodic
                # directions. If it's less than one, then the periodicity in a certain direction is more pronounce.)
                elif len(b) == 2 or len(b) == 3:
                    br = max(b) / min(b)
                # Other cases (no symmetric peaks) are discarded as noisy regions.
                else:
                    br = 0.0
            
            seg[:, x*winres:(x+1)*winres, y*winres:(y+1)*winres, :] = infer * br # br will amplify well-aligned NPs
            ft[x*ftres:(x+1)*ftres, y*ftres:(y+1)*ftres] = imgcrop_ft            # spatial map of the FT

    fig, ax = plt.subplots(3,1,figsize=(24,36))

    ax[0].imshow(d.data, cmap='gray')
    ax[1].imshow(seg[0,:,:,0], cmap='inferno')
    ax[2].imshow(ft, cmap='gray')
    
    plt.tight_layout()
    plt.savefig('{}/frame_{}.png'.format(outfolder,str(fidx).zfill(3)))
    plt.close()

t.stop()
